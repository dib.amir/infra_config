#!/bin/bash

BIN_DIR = '/usr/local/bin/'


sudo add-apt-repository ppa:kelleyk/emacs
sudo apt-get update
sudo apt-get install emacs25

mkdir ~/.emacs.d/packages
git clone https://github.com/jkitchin/scimax.git ~/.emacs.d/packages/scimax/
wget https://raw.githubusercontent.com/jkitchin/scimax/master/ox-word.el ~/.emacs.d/packages/ox-word.el

# magit test
#package
apt-get install $(grep -vE "^\s*#" packages.txt  | tr "\n" " ")
pip3 install --upgrade --user  $(grep -vE "^\s*#" pipackages.txt  | tr "\n" " ")


cat nodepackages.txt | xargs npm install -g


mkdir ~/remote
cp -r scripts ~/

ln -s ~/scripts/sshmount $(BIN_DIR)sshmount

git config --global user.name "Amir Dib"
git config --global user.email "dib.amir@gmail.com"

cp .emacs ~/
# end file
