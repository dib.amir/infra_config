cat applist | xargs brew cask install 
cat brewpackages.txt | xargs brew install

mkdir ~/Documents/temp
git clone https://github.com/powerline/fonts ~/Documents/temp/fonts
cd fonts
.~/Documents/temp/fonts/install.sh
rm -rf ~/Documents/temp/fonts


git clone https://github.com/wesbos/Cobalt2-iterm.git ~/Documents/temp/Cobalt2-iterm
cd ~/Documents/temp/Cobalt2-iterm/cobalt2.zsh-theme ~/.oh-my-zsh/themes/
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
