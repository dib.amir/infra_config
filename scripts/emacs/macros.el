(with-current-buffer (url-retrieve-synchronously "https://iterm2.com/utilities/imgcat")
  (kill-new (buffer-string)))

(defun addEvent (start description location url)
  "Create iCal event"
  (interactive "sstart date: 
sdescription:
slocation:
surl:")
  ;; (message "%s %s %s" start end description)
  (shell-command (format "osascript -e 'tell application \"iCal\" to make new event at end of calendar 1 with properties {start date:date \"%s\",  summary:\"%s\", location:\"%s\",  url:\"%s\"}'" start description location url))
  )
