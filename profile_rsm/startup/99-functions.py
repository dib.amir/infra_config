def info_versions():
    fmt = '{:20s} : {}'
    print(fmt.format('Numpy', np.__version__))
    print(fmt.format('Scipy ', scipy.__version__))
    print(fmt.format('Matplotlib ', mpl.__version__))
    print(fmt.format('Pandas ', pd.__version__))
    print(fmt.format('Scikit-learn ', sklearn.__version__))


def load_custom():
    plt.rcParams['font.family'] = 'Verdana'
    plt.rcParams['figure.figsize'] = [15, 12]
    plt.rcParams['font.size'] = 14
    plt.rcParams['axes.labelsize'] = 2 + plt.rcParams['font.size']
    plt.rcParams['xtick.labelsize'] = plt.rcParams['font.size']
    plt.rcParams['ytick.labelsize'] = plt.rcParams['font.size']
    plt.rcParams['xtick.major.size'] = 6
    plt.rcParams['xtick.minor.size'] = 3
    plt.rcParams['xtick.major.width'] = 1
    plt.rcParams['xtick.minor.width'] = 1
    plt.rcParams['ytick.major.size'] = 6
    plt.rcParams['ytick.minor.size'] = 3
    plt.rcParams['ytick.major.width'] = 1
    plt.rcParams['ytick.minor.width'] = 1
    plt.rcParams['legend.frameon'] = False
