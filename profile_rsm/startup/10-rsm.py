import sys
PROJECT_DIR = '/home/ubuntu/rsm_adb/'
sys.path.append(PROJECT_DIR)

from rsm.tools.dataset import time_decomposition
from rsm.tools.iterable import ParameterGrid
from rsm.tools.reader import read_json
from rsm.tools.database import *

from rsm.main import add_filter, run_experiment, update_preprocess_parameters, test_parameters_for_run, run_grid_experiments
from rsm.data.utils.reader import *
from rsm.data.utils.experiments import parameters_to_dataframe, create_experiment, Experiment, Experiments, create_default_experiment
from reports.rapport2018.scripts.plot import plot_time_between_codes, plot_gaussian_filter, plot_time_distribution, plot_signal_and_ouput_filter

from rsm.visualization.plot import *

config = read_json(
    PROJECT_DIR + 'rsm/config.json')

PARAMETERS_COLLECTION = config['PARAMETERS_COLLECTION']
DATABASE = config["DATABASE"]
DATA_DATABASE = config["DATA_DATABASE"]
HOST = config["HOST"]

completed_experiments = get_completed_experiments_pids()
experiments = Experiments(completed_experiments)
default_parameters = create_default_experiment()
#default_pid = test_parameters_for_run(default_parameters)

def load_test_datasets(pid_parameters):
    experiment = Experiment(pid_parameters)
    cleaned_errors = experiment.get_data(name='errors', state='clean')
    cleaned_breakdowns = experiment.get_data(name='breakdowns', state='clean')
    intermediate_errors = experiment.get_data(name='errors', state='intermediate')
    dataset = experiment.get_data(state='preprocess_pandas')
    X, y = experiment.get_data(state='preprocess')
    X_test, y_test, y_pred = experiment.get_data(state='training')
    clf = experiment.get_data(state='model')
    metrics = experiment.get_data(state='metrics')
    
    return {'experiment': experiment,
            'cleaned_breakdowns': cleaned_breakdowns,
            'cleaned_errors': cleaned_errors,
            'intermediate_errors': intermediate_errors,
            'preprocess_pandas': dataset,
            'preprocess': (X, y),
            'training': (X_test, y_test, y_pred),
            'clf': clf,
            'metrics': metrics
            }


def scores(clf, X_test):
    if hasattr(clf, "decision_function"):
        y_score = clf.decision_function(X_test)
    else:
        y_score = clf.predict_proba(X_test)[:, 0]
    return y_score
