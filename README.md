Contient les éléments de configuration de base pour MacOSX et linux. 


* pippackages.txt: liste des modules python;
* packages.txt: liste des paquets linux;
* brewpackages.txt: liste des applications brew;
* applications.txt: liste des applications Mac OS X;
* .emacs: configuration des paquets emacs.

Mac OS X
===========
```sh
source brewinstall.sh
```

Linux
=========
```sh
source install.sh
```



